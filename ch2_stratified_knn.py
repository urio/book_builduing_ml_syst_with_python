
from scipy import *
from pandas import *
import seaborn as sb

from sklearn.neighbors import KNeighborsClassifier
from sklearn.cross_validation import StratifiedKFold
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

def data_seeds():
    # Classes are numbers, but refer to Canadian, Koma, and Rosa.
    data = read_csv('data/seeds.tab', header=None, sep=r'\t+', engine='python')
    data.columns = ['Area', 'Perimeters', 'Compactness', 'Kernel.Length',
            'Kernel.Width', 'Asymmetry', 'Groove.Length', 'Variety']
    data['Variety'] = data['Variety'].astype('category')
    return data

def knn(data, target_name='Class', nn=1, nfolds=10, norm=None):
    X = data[data.columns[data.columns != target_name]]
    y = data[target_name]
    classifier = KNeighborsClassifier(n_neighbors=nn)
    if norm == 'z-score':
        classifier = Pipeline([('norm', StandardScaler()), ('knn', classifier)])
    folds = StratifiedKFold(y, n_folds=nfolds, shuffle=False)
    means = []
    for training, testing in folds:
        classifier.fit(X.ix[training], y[training])
        prediction = classifier.predict(X.ix[testing])
        means.append(mean(prediction == y[testing]))
    return means

def kfold_knn(data, nn, nfolds, norm):
    accuracies = knn(data, target_name='Variety', nn=nn, nfolds=nfolds,
            norm=norm)
    print('-> mean accuracy: {:.4f} ± {:.4f}'.format(mean(accuracies), std(accuracies)))
    return dict(zip(range(0, nfolds), accuracies))

def test_knn(kNFolds = 10, kNN = range(1, 50)):
    data = data_seeds()
    results = list()
    for norm in [None, 'z-score']:
        for i in kNN:
            acc = kfold_knn(data, nn=i, nfolds=kNFolds, norm=norm)
            df = DataFrame({'k': i, 'Pre-processing': norm or 'None', 'accuracy': acc})
            results.append(df)
    results = concat(results)
    p = sb.tsplot(data=results, time='k', value='accuracy',
            condition='Pre-processing')
    p.set(ylim = (0.6, 1))
    sb.plt.show()

test_knn()

#import seaborn as sb
#
#iris = data_iris()
#sb.pairplot(iris, hue="Species")
#plt.show()
#
#seeds = data_seeds()
#sb.pairplot(seeds, hue="Variety")
#plt.show()
#
