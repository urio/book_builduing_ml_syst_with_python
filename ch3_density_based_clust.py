
from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn import preprocessing

import matplotlib.pyplot as plt
from pandas import *
from numpy import array
import seaborn as sb
sb.set()

def data_seeds():
    # Classes are numbers, but refer to Canadian, Koma, and Rosa.
    data = read_csv('data/seeds.tab', header=None, sep=r'\t+', engine='python')
    data.columns = ['Area', 'Perimeters', 'Compactness', 'Kernel.Length',
            'Kernel.Width', 'Asymmetry', 'Groove.Length', 'Variety']
    data['Variety'] = data['Variety'].astype('category')
    return data

def validate(X, model, y):
    pred = model.labels_
    return {'Homogeneity': [metrics.homogeneity_score(y, pred)],
            'Completeness': [metrics.completeness_score(y, pred)],
            'V-Measure': [metrics.v_measure_score(y, pred)],
            'NMI': [metrics.adjusted_mutual_info_score(y, pred)],
            'ARI': [metrics.adjusted_rand_score(y, pred)]}

def dbscan(X, eps, minPts=3):
    model = DBSCAN(eps=eps, min_samples=minPts)
    model.fit(X)
    return model

def test_dbscan(X, y, eps=[0.1]):
    r = list()
    er = list()
    for e in eps:
        for i in range(2):
            model = dbscan(X, e)
            params = {'eps': e, 'set': i}
            params.update(validate(X, model, y))
            r.append(DataFrame(params))
            params = {'eps': e, 'set': i, 'Number of clusters': [len(unique(model.labels_))]}
            er.append(DataFrame(params))
    results = concat(r)
    results = melt(results, id_vars=['eps', 'set'], var_name='metric',
                    value_name='score')

    fig, ax1 = plt.subplots()
    p1 = sb.tsplot(data=results, time='eps', unit='set', value='score',
                  condition='metric', ax=ax1)

    results = concat(er)
    results = melt(results, id_vars=['eps', 'set'], var_name='k',
            value_name='Number of clusters')
    p2 = sb.tsplot(data=results, time='eps', unit='set', value='Number of clusters',
            condition='k', ax=ax1.twinx(), linestyle='--')

    p1.set_yticks(np.linspace(p1.get_ybound()[0], p1.get_ybound()[1], 8))
    p2.set_yticks(np.linspace(p2.get_ybound()[0], p2.get_ybound()[1], 8))
    h1, l1 = p1.get_legend_handles_labels()
    h2, l2 = p2.get_legend_handles_labels()
    p1.legend(h1+h2, l1+l2, loc=2)
    p1.set_title("DBSCAN results for seeds dataset (3 real classes)")

    sb.plt.show()

df = data_seeds()
X = preprocessing.scale(df.ix[:, range(7)])
y = df.ix[:, 7].cat.codes

test_dbscan(X, y, eps=(array(range(0, 1000, 2))+1)/1000)

