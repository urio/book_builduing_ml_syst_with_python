
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score, silhouette_samples
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk.stem

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np

plt.style.use("ggplot")

from util import *

english_stemmer = nltk.stem.SnowballStemmer('english')

class StemmedTfidfVectorizer(TfidfVectorizer):
    def build_analyzer(self):
        analyzer = super(StemmedTfidfVectorizer, self).build_analyzer()
        return lambda doc: (english_stemmer.stem(w) for w in analyzer(doc))

kGroups = ['comp.graphics', 'comp.os.ms-windows.misc',
        'comp.sys.ibm.pc.hardware', 'comp.sys.mac.hardware', 'comp.windows.x',
        'sci.space']

def load():
    train_data = data_20newsgroups(subset='train', categories=kGroups)
    test_data = data_20newsgroups(subset='test', categories=kGroups)
    vectorizer = StemmedTfidfVectorizer(min_df=10, max_df=0.5,
            stop_words='english', decode_error='ignore')
    train_vectorized = vectorizer.fit_transform(train_data.data)
    test_vectorized = vectorizer.fit_transform(train_data.data)
    #num_samples, num_features = vectorized.shape
    #print("#samples: %d, #features: %d" % (num_samples, num_features))
    return (train_vectorized, test_vectorized)

def kmeans(data, nclusters = 10):
    km = KMeans(n_clusters=nclusters, init='random', n_init=1, random_state=3)
    km.fit(data)
    return km

# Based on
# http://scikit-learn.org/stable/auto_examples/cluster/plot_kmeans_silhouette_analysis.html
def kmeans_plot(X, nclusters, silhouette_x, silhouette_y):
    model = kmeans(train, nclusters)
     # Create a subplot with 1 row and 2 columns
    fig, (ax0, ax1, ax2) = plt.subplots(1, 3)
    fig.set_size_inches(18, 7)

    ax0.plot(silhouette_x, silhouette_y)

    # The 1st subplot is the silhouette plot
    # The silhouette coefficient can range from -1, 1 but in this example all
    # lie within [-0.1, 1]
    ax1.set_xlim([-.6, +0.7])
    # The (n_clusters+1)*10 is for inserting blank space between silhouette
    # plots of individual clusters, to demarcate them clearly.
    ax1.set_ylim([0, X.shape[0] + (nclusters + 1) * 10])

    # Initialize the model with nclusters value and a random generator
    # seed of 10 for reproducibility.
    model = kmeans(X, nclusters)
    cluster_labels = model.labels_

    # The silhouette_score gives the average value for all the samples.
    # This gives a perspective into the density and separation of the formed
    # clusters
    silhouette_avg = silhouette_score(X, cluster_labels)
    print("For nclusters =", nclusters,
          "The average silhouette_score is :", silhouette_avg)

    # Compute the silhouette scores for each sample
    sample_silhouette_values = silhouette_samples(X, cluster_labels)

    y_lower = 10
    for i in range(nclusters):
        # Aggregate the silhouette scores for samples belonging to
        # cluster i, and sort them
        ith_cluster_silhouette_values = \
            sample_silhouette_values[cluster_labels == i]

        ith_cluster_silhouette_values.sort()

        size_cluster_i = ith_cluster_silhouette_values.shape[0]
        y_upper = y_lower + size_cluster_i

        color = cm.spectral(float(i) / nclusters)
        ax1.fill_betweenx(np.arange(y_lower, y_upper),
                          0, ith_cluster_silhouette_values,
                          facecolor=color, edgecolor=color, alpha=0.7)

        # Label the silhouette plots with their cluster numbers at the middle
        ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

        # Compute the new y_lower for next plot
        y_lower = y_upper + 10  # 10 for the 0 samples

    ax1.set_title("The silhouette plot for the various clusters.")
    ax1.set_xlabel("The silhouette coefficient values")
    ax1.set_ylabel("Cluster label")

    # The vertical line for average silhoutte score of all the values
    ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

    ax1.set_yticks([])  # Clear the yaxis labels / ticks
    ax1.set_xticks([-0.6, -0.4, -0.2, 0, 0.2, 0.4, 0.6, 0.7])

    # 2nd Plot showing the actual clusters formed
    colors = cm.spectral(cluster_labels.astype(float) / nclusters)
    ax2.scatter(X[:, 0].todense(), X[:, 1].todense(), marker='.', s=30,
            alpha=0.7, c=colors)

    # Labeling the clusters
    centers = model.cluster_centers_
    # Draw white circles at cluster centers
    ax2.scatter(centers[:, 0], centers[:, 1],
                marker='o', c="white", alpha=1, s=200)

    for i, c in enumerate(centers):
        ax2.scatter(c[0], c[1], marker='$%d$' % i, alpha=1, s=50)

    ax2.set_title("The visualization of the clustered data.")
    ax2.set_xlabel("Feature space for the 1st feature")
    ax2.set_ylabel("Feature space for the 2nd feature")

    plt.suptitle(("Silhouette analysis for KMeans clustering on sample data "
                  "with nclusters = %d" % nclusters),
                 fontsize=14, fontweight='bold')

    plt.show()

def kmeans_test(data, nclusters=[2, 3, 4]):
    s = [silhouette_score(data, kmeans(data, nc).labels_) for nc in nclusters]
    best = nclusters[int(np.argmax(s))]
    print('Silhouettes:', s)
    print('Best: k =', best, ' - value =', np.max(s))
    kmeans_plot(data, best, nclusters, s)

train, test = load()
#kmeans_test(train, nclusters=[50])
kmeans_test(train, nclusters=range(10, 201, 10))

