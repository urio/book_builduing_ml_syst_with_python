
from scipy.cluster import hierarchy
import matplotlib.pyplot as plt
plt.style.use("ggplot")

from pandas import *

def data_iris():
    data = read_csv('data/iris.csv', delimiter=',', header=None)
    data.columns = ['Sepal.Length', 'Sepal.Width', 'Petal.Length',
            'Petal.Width', 'Species']
    data['Species'] = data['Species'].astype('category')
    return data

# Plot dendrogram for the results of many methods
def scipy_hclust(X, k=3):
    kLinkage = ['single', 'complete', 'average', 'weighted', 'centroid',
            'median', 'ward']
    fig, subplots = plt.subplots(1, len(kLinkage))
    for linkage, sp in zip(kLinkage, subplots):
        sp.set_title(linkage)
        model = hierarchy.linkage(X, linkage)
        T = hierarchy.fcluster(model, k, 'maxclust')
        # calculate labels
        labels=list('' for i in range(len(X)))
        for i in range(len(X)):
            labels[i]=str(i)+ ',' + str(T[i])
        # calculate color threshold
        ct = model[-(k - 1), 2]
        dn = hierarchy.dendrogram(model, labels=labels, color_threshold=ct, ax=sp)
    plt.show()

# Compute the confusion matrix for a result with sklearn
def sklearn_hclust(X, y, k=3, methods=['ward', 'complete', 'average']):
    from sklearn.cluster import AgglomerativeClustering
    from sklearn.metrics import confusion_matrix, adjusted_rand_score
    for method in methods:
        model = AgglomerativeClustering(n_clusters=k, linkage=method)
        model.fit(X)
        print('Linkage:', method)
        print('Confusion matrix')
        print(confusion_matrix(y, model.labels_))
        print('ARI:', adjusted_rand_score(y, model.labels_))

def seaborn_hclust(X):
    import seaborn
    from scipy.spatial.distance import pdist, squareform
    seaborn.clustermap(squareform(pdist(X)))
    seaborn.plt.show()

df = data_iris()
X = df.ix[:, range(4)]
y = df.ix[:, 4].cat.codes

#scipy_hclust(X)
sklearn_hclust(X, y)
#seaborn_hclust(X)

