
from pandas import *

def data_iris():
    data = read_csv('data/iris.csv', delimiter=',', header=None)
    data.columns = ['Sepal.Length', 'Sepal.Width', 'Petal.Length',
            'Petal.Width', 'Species']
    data['Species'] = data['Species'].astype('category')
    return data

def data_seeds():
    # Classes are numbers, but refer to Canadian, Koma, and Rosa.
    data = read_csv('data/seeds.tab', header=None, sep=r'\t+', engine='python')
    data.columns = ['Area', 'Perimeters', 'Compactness', 'Kernel.Length',
            'Kernel.Width', 'Asymmetry', 'Groove.Length', 'Variety']
    data['Variety'] = data['Variety'].astype('category')
    return data

def data_20newsgroups(subset='all', **kwargs):
    import sklearn.datasets
    data = sklearn.datasets.fetch_20newsgroups(**kwargs, subset=subset,
                                               data_home='./data/379')
    return data

