from pandas import *
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation as LDA

kGroups = ['comp.graphics', 'comp.os.ms-windows.misc',
        'comp.sys.ibm.pc.hardware', 'comp.sys.mac.hardware', 'comp.windows.x',
        'sci.space']

def data_20newsgroups(subset='all', **kwargs):
    import sklearn.datasets
    data = sklearn.datasets.fetch_20newsgroups(**kwargs, subset=subset,
                                               data_home='./data/379')
    return data

def load():
    train_data = data_20newsgroups(subset='train', categories=kGroups)
    vectorizer = TfidfVectorizer(min_df=2, max_df=0.95,
            stop_words='english', decode_error='ignore')
    return vectorizer.fit_transform(train_data.data)



