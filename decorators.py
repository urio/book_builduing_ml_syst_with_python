
class PrintArgs(object):
    def __init__(self, arg_filter=None):
        self.filter = arg_filter

    def __call__(self, f):
        def wrapper(*fargs, **fkwargs):
            arg_names = f.__code__.co_varnames[:f.__code__.co_argcount]
            args = fargs[:len(arg_names)]
            defaults = f.__defaults__ or ()
            args = args + defaults[len(defaults) - (f.__code__.co_argcount - len(args)):]
            params = dict(zip(arg_names, args))
            args = fargs[len(arg_names):]
            if args: params.update(args)
            if fkwargs: params.update(fkwargs)
            lst = list()
            for k, v in params.items():
                if not self.filter or k in self.filter:
                    lst.append('%s = %r' % (k, v))
            print(f.__name__ + '(' + ', '.join(lst) + ')')
            return f(*fargs, **fkwargs)
        return wrapper

def trace(func):
    '''Decorator to print function call details - parameters names and effective values'''
    def wrapper(*func_args, **func_kwargs):
        print(func.__name__)
        return func(*func_args, **func_kwargs)
    return wrapper

