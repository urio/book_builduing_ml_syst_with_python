
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from scipy.misc import imread
from numpy import uint8

def color_compression(image, ncolors = 8):
    X = (image / 255.0).reshape(-1, 3)
    model = KMeans(ncolors)
    labels = model.fit_predict(X)
    colors = model.cluster_centers_
    new_image = colors[labels].reshape(image.shape)
    new_image = (255 * new_image).astype(uint8)
    return new_image

image = imread("data/brazil.png")
fig, (ax1, ax2) = plt.subplots(1, 2)
ax1.imshow(image)
ax2.imshow(color_compression(image, 16))
plt.grid(False);
plt.show()

