
from scipy import *
import matplotlib.pyplot as plt

from decorators import trace

@trace
def scatter_plot(x, y):
    plt.scatter(x, y, s=10)
    plt.title("Web traffic over the last month")
    plt.xlabel("Time")
    plt.ylabel("Hits/hour")
    plt.xticks([w*7*24 for w in range(10)], ['week %i' % w for w in range(10)])
    plt.autoscale(tight=True)
    # draw a slightly opaque, dashed grid
    plt.grid(True, linestyle='-', color='0.75')

def error(f, x, y):
    return sum((f(x)-y)**2)

@trace
def plot_polyfit(x, y, degree=1):
    f2p = polyfit(x, y, degree)
    print(f2p)
    f2 = poly1d(f2p)
    print(error(f2, x, y))
    fx = linspace(0,x[-1], 1000) # generate X-values for plotting
    plt.plot(fx, f2(fx), linewidth=2)


data = genfromtxt('data/web_traffic.tsv', delimiter='\t')

x, y = data[:, 0], data[:, 1]
x = x[~isnan(y)]
y = y[~isnan(y)]

def with_all():
    scatter_plot(x, y)
    for deg in [1, 2, 3, 10, 53]:
        plot_polyfit(x, y, deg)

def with_inflection_pt(week = 3.5):
    inflection = week * 7 * 24 # calculate the inflection point in hours
    xa = x[ :inflection] # data before the inflection point
    ya = y[ :inflection]
    xb = x[inflection: ] # data after
    yb = y[inflection:]
    fa = poly1d(polyfit(xa, ya, 1))
    fb = poly1d(polyfit(xb, yb, 1))
    fa_error = error(fa, xa, ya)
    fb_error = error(fb, xb, yb)
    print("Error inflection=%f" % (fa_error + fb_error))
    fx = linspace(0, x[-1], len(x)) # generate X-values for plotting
    fxa = fx[ :inflection]
    fxb = fx[inflection: ]
    plt.plot(fxa, fa(fxa), linewidth=2)
    plt.plot(fxb, fb(fxb), linewidth=2)


def predict(week = 3.5):
    inflection = week * 7 * 24 # calculate the inflection point in hours
    xa = x[ :inflection] # data before the inflection point
    ya = y[ :inflection]
    xb = x[inflection: ] # data after
    yb = y[inflection:]
    train = array(range(int(len(xb) * 0.9)))
    fbt2 = poly1d(polyfit(xb[train], yb[train], 2))
    print("fbt2(x)= \n%s" % fbt2)
    print("fbt2(x)-100,000= \n%s" % (fbt2-100000))
    from scipy.optimize import fsolve
    reached_max = fsolve(fbt2-100000, x0=800)/(7*24)
    print("100,000 hits/hour expected at week %f" % reached_max[0])

scatter_plot(x, y)
#with_all()
with_inflection_pt()
predict()
plt.show()

